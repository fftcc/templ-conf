#!/usr/bin/env bash
# DOTS FOR TERMUX

# DISCLAIMER: THE SCRIPT IS WRITTEN EXCLUSIVELY
# FOR THE PERSONAL PURPOSES OF THE AUTHOR.
# USE AT YOUR OWN RISK ! ! !

# +----------------------------------------+
# | Autor    | fftcc                       |
# | License  | MIT                         |
# | Website  | ff99cc.art/                 |
# | E-mail   | me@ff99cc.art               |
# | Git      | codeberg.org/fftcc/         |
# | Keyoxide | keyoxide.org/me@ff99cc.art/ |
# +----------------------------------------+

# VARS
home=$HOME
SCRIPTPATH=`pwd`
my_repo=https://codeberg.org/fftcc/templ-conf/raw/branch/main

pkg upgrade && pkg i mc git gnupg zsh micro vim \
openssh openssl curl wget dnsutils root-repo \
x11-repo nodejs tsu

# INSTALL OH MY ZSH
install_omz() {
	sh -c "$(curl -fsSL https://github.com/Cabbagec/termux-ohmyzsh/raw/master/install.sh)" &&
	git clone https://github.com/zsh-users/zsh-completions ${ZSH_CUSTOM:-${ZSH:-~/.oh-my-zsh}/custom}/plugins/zsh-completions &&
	git clone https://github.com/zsh-users/zsh-syntax-highlighting ${ZSH_CUSTOM:-${ZSH:-~/.oh-my-zsh}/custom}/plugins/zsh-syntax-highlighting &&
	git clone https://github.com/zsh-users/zsh-autosuggestions ${ZSH_CUSTOM:-${ZSH:-~/.oh-my-zsh}/custom}/plugins/zsh-autosuggestions
}

tconf() {
	cd .termux
	cp colors.properties font.ttf ~/.termux
	cd - &>/dev/null
}

# DOWNLOADS DOTFILES
dwn_dots() {
    curl -OL $my_repo/.zshrc
    curl -OL $my_repo/.gitconfig
    cd .ssh
    curl -OL $my_repo/.ssh/config
}

install_omz
tconf

if [ "$SCRIPTPATH" = "$home" ]; then
    dwn_dots
else
    cd $home && dwn_dots && cd - &>/dev/null
fi
