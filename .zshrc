export ZSH="$HOME/.oh-my-zsh"

ZSH_THEME="af-magic"

plugins=(git extract zsh-syntax-highlighting zsh-autosuggestions zsh-completions)

source $ZSH/oh-my-zsh.sh

# ALIASES
alias re="reboot"
alias sdn="shutdown -h now"
alias ex="exit"
alias cl="clear"
alias l="ls -a"
alias ll="ls -la"
alias m="micro"

# PASS
alias up="upass"
alias p="pass"
alias pi="pass insert -m"
alias pe="pass edit"
alias ppush="pass git push"
alias ppull="pass git pull"
alias pp="pass git pull && pass git push"
alias pc="pass -c"
alias po="pass otp -c"
alias poq="pass otp uri -q"

# TMUX
alias t="tmux"
alias ta="tmux attach -t"
alias tn="tmux new-session -s"
alias th="tmux split-window -h"
alias tv="tmux split-window -v"
alias td="tmux detach"
#########################################

# SSH AGENT AUTO
#eval `ssh-agent -s` >/dev/null
#export SSH_AUTH_SOCK

# GPG AGENT with SSH
#unset SSH_AGENT_PID
#if [ "${gnupg_SSH_AUTH_SOCK_by:-0}" -ne $$ ]; then
#  export SSH_AUTH_SOCK="$(gpgconf --list-dirs agent-ssh-socket)"
#fi
#export GPG_TTY=$(tty)
#gpg-connect-agent updatestartuptty /bye >/dev/null && gpg-connect-agent updatestartuptty /bye >/dev/null

# GPG AGENT without SSH
#export GPG_TTY=$(tty)
#gpg-agent - starting '/data/data/com.termux/files/usr/bin/gpg-agent' >/dev/null
#gpg-connect-agent reloadagent /bye >/dev/null && gpg-connect-agent updatestartuptty /bye >/dev/null
